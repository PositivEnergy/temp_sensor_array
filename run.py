
import random, time
import json, httplib
from smbus import SMBus
import sys
# temp sensor
sys.path.append('gfdiPiTools/customLibraries')
import GFDITools
# light sensor
sys.path.append('TSL2561')
sys.path.append('gfdiPiTools/thirdPartyLibraries')
from TSL2561 import *

#from Adafruit_I2C import Adafruit_I2C

# read parse keys from text file. Don't save in git
#f = open('parse_keys.txt', 'r')
#parse_keys = f.read()
#parse_keys = parse_keys.split("\n")
#parse_keys = [i[i.find(":")+1:] for i in parse_keys]
#parse_keys = {"app_id":parse_keys[0], "api_key":parse_keys[1]}

def post_data_to_parse(data_dict, app_id, api_key):
  headers={}
  headers["X-Parse-Application-Id"] = app_id
  headers["X-Parse-REST-API-Key"] = api_key
  headers["Content-Type"] = " application/json"
  conn = httplib.HTTPSConnection('api.parse.com', 443)
  conn.connect()
  conn.request('POST', '/1/classes/envsensor', json.dumps(data_dict), headers)
  result = json.loads(conn.getresponse().read())
  return(result)

#for i in range(1, 5):
#  for j in range(1, 5):
#    pi = i
#    node = j
#    temp = round(random.uniform(70, 75), 2)
#    hum = round(random.uniform(10, 30), 2)
#    lux = round(random.uniform(80, 120), 2)
#    data_dict = {"pi":pi, "node":node, "temp":temp, "hum":hum, "lux":lux}
#    result = post_data_to_parse(data_dict, parse_keys["app_id"], parse_keys["api_key"])
#    print("pi: " + str(pi) + " node: " + str(node) + " result: ")
#    print(result)

# temp sensor
#bus = GFDITools.guessBus()
#HIH6130 = SMBus(bus=bus)
# Temporary storage array for HIH6130 data
#blockData = [0, 0, 0, 0]
#HIH6130.write_quick(0x27)
# Wait for it.
#time.sleep(0.050)
# Read the data we requested.
#blockData = HIH6130.read_i2c_block_data(0x27, 0)
# Process the data.
#status = (blockData[0] & 0xc0) >> 6
#humidity = (((blockData[0] & 0x3f) << 8) + blockData[1]) * 100.0 / 16383.0
#tempC2 = ((blockData[2] << 6) + ((blockData[3] & 0xfc) >> 2)) * 165.0 / 16383.0 - 40.0
#tempF2 = tempC2*9.0/5.0 + 32.0

# light sensor
tsl1 = TSL2561(busnum=0)
full_lum1 = tsl1.readLux()

tsl2 = TSL2561(busnum=1)
full_lum2 = tsl2.readLux()

#print(bus)
#print("humidity " + str(humidity))
#print("tempC2 " + str(tempC2))
#print("tempF2 " + str(tempF2))
print("lux " + str(full_lum1))
print("lux " + str(full_lum2))

